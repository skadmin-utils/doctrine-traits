<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Website
{
    #[ORM\Column]
    private string $website = '';

    public function getWebsite(): string
    {
        return $this->website;
    }
}

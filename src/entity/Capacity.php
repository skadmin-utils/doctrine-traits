<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Capacity
{
    #[ORM\Column]
    private int $capacity = 0;

    public function getCapacity(): int
    {
        return $this->capacity;
    }
}

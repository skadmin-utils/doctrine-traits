<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function trim;

trait Nickname
{
    #[ORM\Column]
    private string $nickname = '';

    public function getNickname(): string
    {
        return trim($this->nickname);
    }
}

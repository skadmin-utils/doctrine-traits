<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait Birthdate
{
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $birthdate = null;

    public function getBirthdate(?string $format = null): DateTimeInterface|string|null
    {
        if ($format === null || $this->birthdate === null) {
            return $this->birthdate;
        }

        return $this->birthdate->format($format);
    }
}

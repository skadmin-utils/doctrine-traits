<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Value
{
    #[ORM\Column]
    private string $value = '';

    public function getValue(): string
    {
        return $this->value;
    }
}

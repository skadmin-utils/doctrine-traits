<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\HasLifecycleCallbacks]
trait LastUpdateDate
{
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private DateTimeInterface $lastUpdateAt;

    public function getLastUpdateAt(): DateTimeInterface
    {
        return $this->lastUpdateAt;
    }

    #[ORM\PreUpdate]
    #[ORM\PrePersist]
    public function onPreUpdateLastUpdateDate(): void
    {
        $this->lastUpdateAt = new DateTime();
    }
}

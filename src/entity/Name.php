<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Name
{
    #[ORM\Column]
    private string $name = '';

    public function getName(): string
    {
        return $this->name;
    }
}

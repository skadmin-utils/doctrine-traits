<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\HasLifecycleCallbacks]
trait LastUpdate
{
    #[ORM\Column]
    private string $lastUpdateAuthor;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private DateTimeInterface $lastUpdateAt;

    public function getLastUpdateAuthor(): string
    {
        return $this->lastUpdateAuthor;
    }

    public function getLastUpdateAt(): DateTimeInterface
    {
        return $this->lastUpdateAt;
    }

    #[ORM\PreUpdate]
    #[ORM\PrePersist]
    public function onPreUpdateLastUpdate(): void
    {
        $this->lastUpdateAt = new DateTime();
    }
}

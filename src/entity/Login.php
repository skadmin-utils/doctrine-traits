<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Login
{
    #[ORM\Column]
    private string $userName;

    #[ORM\Column]
    private string $userPassword;

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function getUserPassword(): string
    {
        return $this->userPassword;
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function boolval;

trait IsPublic
{
    #[ORM\Column(options: ['default' => false])]
    private bool $isPublic = false;

    public function isPublic(): bool
    {
        return boolval($this->isPublic);
    }

    public function setIsPublic(bool|int $isPublic): void
    {
        $this->isPublic = boolval($isPublic);
    }
}

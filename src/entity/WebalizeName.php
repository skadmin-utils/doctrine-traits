<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;

use function trim;

#[ORM\HasLifecycleCallbacks]
trait WebalizeName
{
    use Name;

    #[ORM\Column]
    private string $webalize = '';

    public function getWebalize(): string
    {
        return $this->webalize;
    }

    public function setWebalize(string $webalize): void
    {
        if (trim($this->webalize) !== '') {
            return;
        }

        $this->webalize = $webalize;
    }

    public function updateWebalize(string $webalize): void
    {
        $this->webalize = $webalize;
    }

    #[ORM\PreUpdate]
    #[ORM\PrePersist]
    public function onPreUpdateWebalizeName(): void
    {
        $this->setWebalize(Strings::webalize($this->getName()));
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait NotePrivate
{
    #[ORM\Column(type: Types::TEXT)]
    private string $notePrivate = '';

    public function getNotePrivate(): string
    {
        return $this->notePrivate;
    }
}

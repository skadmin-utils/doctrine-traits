<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function boolval;

trait IsValid
{
    #[ORM\Column(options: ['default' => false])]
    private bool $isValid = false;

    public function isValid(): bool
    {
        return boolval($this->isValid);
    }

    public function setIsValid(bool|int $isValid): void
    {
        $this->isValid = boolval($isValid);
    }
}

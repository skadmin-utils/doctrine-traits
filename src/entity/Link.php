<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Link
{
    #[ORM\Column]
    private string $link;

    public function getLink(): string
    {
        return $this->link;
    }
}

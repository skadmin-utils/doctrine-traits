<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait ImagePreview
{
    #[ORM\Column(nullable: true)]
    private ?string $imagePreview = '';

    public function getImagePreview(): ?string
    {
        if ($this->imagePreview !== null) {
            return $this->imagePreview === '' ? null : $this->imagePreview;
        }

        return $this->imagePreview;
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait Description
{
    #[ORM\Column(type: Types::TEXT)]
    private string $description;

    public function getDescription(): string
    {
        return $this->description;
    }
}

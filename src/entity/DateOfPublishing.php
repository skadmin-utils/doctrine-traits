<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait DateOfPublishing
{
    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private DateTimeInterface $dateOfPublishing;

    public function getDateOfPublishing(): DateTimeInterface
    {
        return $this->dateOfPublishing;
    }
}

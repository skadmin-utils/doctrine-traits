<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait FinancialReward
{
    #[ORM\Column]
    private int $financialReward = 0;

    public function getFinancialReward(): int
    {
        return $this->financialReward;
    }
}

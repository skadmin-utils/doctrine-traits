<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function boolval;

trait IsPublicFacebook
{
    use Facebook;

    #[ORM\Column(options: ['default' => false])]
    private bool $isPublicFacebook = false;

    public function getFacebook(bool $force = false): string
    {
        if ($force || $this->isPublicFacebook()) {
            return $this->facebook;
        }

        return '';
    }

    public function isPublicFacebook(): bool
    {
        return boolval($this->isPublicFacebook);
    }

    public function setIsPublicFacebook(bool|int $isPublicFacebook): void
    {
        $this->isPublicFacebook = boolval($isPublicFacebook);
    }
}

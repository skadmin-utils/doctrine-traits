<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait StaticUserData
{
    #[ORM\Column]
    private string $staticName = '';

    #[ORM\Column]
    private string $staticEmail = '';

    public function getStaticName(): string
    {
        return $this->staticName;
    }

    public function getStaticEmail(): string
    {
        return $this->staticEmail;
    }
}

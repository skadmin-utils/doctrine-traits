<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Facebook
{
    #[ORM\Column]
    private string $facebook = '';

    public function getFacebook(): string
    {
        return $this->facebook;
    }
}

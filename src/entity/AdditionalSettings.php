<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait AdditionalSettings
{
    /** @var string[]|bool */
    #[ORM\Column(type: Types::ARRAY)]
    private array|bool $additionalSettings = [];

    /**
     * @return string[]|array
     */
    public function getAdditionalSettings(): array
    {
        if ($this->additionalSettings === false) { // @phpstan-ignore-line
            $this->additionalSettings = [];
        }

        return $this->additionalSettings;
    }

    public function getAdditionalSetting(string $key, ?string $default = null): ?string
    {
        if (! isset($this->additionalSettings[$key])) {
            return $default;
        }

        return $this->additionalSettings[$key];
    }

    /**
     * @param string[]|array $additionalSettings
     */
    public function setAdditionalSettings(array $additionalSettings): void
    {
        $this->additionalSettings = $additionalSettings;
    }

    public function setAdditionalSetting(string $key, mixed $value): void
    {
        $this->additionalSettings[$key] = $value;
    }
}

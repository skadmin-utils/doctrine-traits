<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait Content
{
    #[ORM\Column(type: Types::TEXT)]
    private string $content;

    public function getContent(): string
    {
        return $this->content;
    }
}

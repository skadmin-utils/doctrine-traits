<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function sprintf;

trait PlaceOnTheMap
{
    #[ORM\Column]
    private string $placeName = '';

    #[ORM\Column]
    private string $placeAddress = '';

    #[ORM\Column]
    private string $placeGpsLat = ''; // 49.560852

    #[ORM\Column]
    private string $placeGpsLng = ''; // 16.291169

    public function getPlaceName(): string
    {
        return $this->placeName;
    }

    public function getPlaceAddress(): string
    {
        return $this->placeAddress;
    }

    public function getPlaceLinkToMap(): string
    {
        return sprintf(
            'https://mapy.cz/zakladni?x=%s&y=%s&z=14&source=coor&id=%s,%s',
            $this->getPlaceGpsLng(),
            $this->getPlaceGpsLat(),
            $this->getPlaceGpsLng(),
            $this->getPlaceGpsLat()
        );
    }

    public function getPlaceGpsLng(): string
    {
        return $this->placeGpsLng;
    }

    public function getPlaceGpsLat(): string
    {
        return $this->placeGpsLat;
    }

    public function isSetPlace(): bool
    {
        return $this->getPlaceGpsLng() !== '' && $this->getPlaceGpsLat() !== '';
    }
}

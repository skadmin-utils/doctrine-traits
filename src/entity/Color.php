<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Color
{
    #[ORM\Column]
    private string $color = '';

    public function getColor(): string
    {
        return $this->color;
    }
}

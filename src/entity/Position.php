<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Position
{
    #[ORM\Column]
    private string $position = '';

    public function getPosition(): string
    {
        return $this->position;
    }
}

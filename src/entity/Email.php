<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Validators;

trait Email
{
    #[ORM\Column]
    private string $email = '';

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = Validators::isEmail($email) ? $email : '';
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Counter
{
    #[ORM\Column]
    private int $counter = 1;

    public function getCounter(): int
    {
        return $this->counter;
    }
}

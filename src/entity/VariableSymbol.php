<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait VariableSymbol
{
    #[ORM\Column]
    private string $variableSymbol = '';

    public function getVariableSymbol(): string
    {
        return $this->variableSymbol;
    }

    public function setVariableSymbol(string $variableSymbol): void
    {
        if ($this->variableSymbol !== '') {
            return;
        }

        $this->variableSymbol = $variableSymbol;
    }
}

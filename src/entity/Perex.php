<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait Perex
{
    #[ORM\Column(type: Types::TEXT)]
    private string $perex;

    public function getPerex(): string
    {
        return $this->perex;
    }
}

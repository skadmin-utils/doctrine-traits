<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

use function sprintf;

trait File
{
    use Name;
    use Hash;
    use Created;

    #[ORM\Column(nullable: true)]
    private ?string $identifier = '';

    #[ORM\Column]
    private int $size = 0;

    #[ORM\Column]
    private string $mimeType = '';

    #[ORM\Column(type: Types::DATETIME_MUTABLE, options: ['default' => 'CURRENT_TIMESTAMP'])]
    private DateTimeInterface $lastUpdateAt;

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function getSizeInUnit(): string
    {
        $size = $this->size;

        foreach (['kB', 'MB', 'GB', 'TB'] as $unit) {
            $size /= 1024;

            if ($size < 1024) {
                return sprintf('%.02f %s', $size, $unit);
            }
        }

        return sprintf('%.02f %s', $size, $unit ?? 'B');
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    public function getLastUpdateAt(): DateTimeInterface
    {
        return $this->lastUpdateAt;
    }

    #[ORM\PreUpdate]
    public function onPreUpdateFile(): void
    {
        $this->lastUpdateAt = new DateTime();
    }

    #[ORM\PrePersist]
    public function onPrePersistFile(): void
    {
        $this->lastUpdateAt = new DateTime();
    }
}

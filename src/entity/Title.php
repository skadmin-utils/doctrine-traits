<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Title
{
    #[ORM\Column]
    private string $title;

    public function getTitle(): string
    {
        return $this->title;
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function sprintf;

trait Address
{
    #[ORM\Column]
    private string $address = '';

    public function getAddressLinkToMap(): string
    {
        return sprintf(
            'https://mapy.cz/zakladni?q=%s',
            $this->getAddress()
        );
    }

    public function getAddress(): string
    {
        return $this->address;
    }
}

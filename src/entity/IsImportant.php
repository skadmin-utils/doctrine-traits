<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function boolval;

trait IsImportant
{
    #[ORM\Column(options: ['default' => false])]
    private bool $isImportant = false;

    public function isImportant(): bool
    {
        return boolval($this->isImportant);
    }

    public function setIsImportant(bool|int $isImportant): void
    {
        $this->isImportant = boolval($isImportant);
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait ForgotPassword
{
    #[ORM\Column(nullable: true, length: 32)]
    private ?string $forgotPasswordHash = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $forgotPasswordDate = null;

    public function getForgotPasswordHash(): ?string
    {
        return $this->forgotPasswordHash;
    }

    public function setForgotPasswordHash(string $forgotPasswordHash): void
    {
        $this->forgotPasswordHash = $forgotPasswordHash;
        $forgotPasswordDate       = new DateTime();
        $forgotPasswordDate->modify('+1 hour');
        $this->forgotPasswordDate = $forgotPasswordDate;
    }

    public function getForgotPasswordDate(): ?DateTimeInterface
    {
        return $this->forgotPasswordDate;
    }

    public function resetForgotPassword(): void
    {
        $this->forgotPasswordHash = null;
        $this->forgotPasswordDate = null;
    }
}

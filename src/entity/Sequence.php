<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Sequence
{
    #[ORM\Column]
    private ?int $sequence = null;

    public function getSequence(): ?int
    {
        return $this->sequence;
    }

    public function setSequence(int $sequence): void
    {
        if ($this->sequence !== null) {
            return;
        }

        $this->sequence = $sequence;
    }

    public function updateSequence(int $sequence): void
    {
        $this->sequence = $sequence;
    }
}

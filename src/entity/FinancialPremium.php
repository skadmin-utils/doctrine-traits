<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait FinancialPremium
{
    #[ORM\Column]
    private string $financialPremium = '';

    public function getFinancialPremium(): string
    {
        return $this->financialPremium;
    }
}

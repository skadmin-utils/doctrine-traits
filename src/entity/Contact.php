<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Contact
{
    use Email;

    #[ORM\Column]
    private string $phone = '';

    public function getPhone(): string
    {
        return $this->phone;
    }
}

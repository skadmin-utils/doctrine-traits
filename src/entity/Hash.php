<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function trim;

trait Hash
{
    #[ORM\Column]
    private string $hash = '';

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash): void
    {
        if (trim($this->hash) !== '') {
            return;
        }

        $this->hash = $hash;
    }
}

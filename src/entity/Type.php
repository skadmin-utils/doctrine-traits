<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Type
{
    #[ORM\Column]
    private string $type = '';

    public function getType(): string
    {
        return $this->type;
    }
}

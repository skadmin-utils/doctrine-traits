<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Number
{
    #[ORM\Column]
    private int $number = 0;

    public function getNumber(): int
    {
        return $this->number;
    }
}

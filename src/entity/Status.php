<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait Status
{
    #[ORM\Column]
    private int $status;

    public function getStatus(): ?int
    {
        return $this->status;
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function trim;

trait Code
{
    #[ORM\Column]
    private string $code = '';

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        if (trim($this->code) !== '') {
            return;
        }

        $this->code = $code;
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait OrderNumber
{
    #[ORM\Column]
    private string $orderNumber = '';

    public function getOrderNumber(): string
    {
        return $this->orderNumber;
    }

    public function setOrderNumber(string $orderNumber): void
    {
        if ($this->orderNumber !== '') {
            return;
        }

        $this->orderNumber = $orderNumber;
    }
}

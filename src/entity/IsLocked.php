<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function boolval;

trait IsLocked
{
    #[ORM\Column(options: ['default' => false])]
    private bool $isLocked = false;

    public function isLocked(): bool
    {
        return boolval($this->isLocked);
    }

    public function setIsLocked(bool|int $isLocked): void
    {
        $this->isLocked = boolval($isLocked);
    }
}

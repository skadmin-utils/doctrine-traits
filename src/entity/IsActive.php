<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function boolval;

trait IsActive
{
    #[ORM\Column(options: ['default' => true])]
    private bool $isActive = true;

    public function isActive(): bool
    {
        return boolval($this->isActive);
    }

    public function setIsActive(bool|int $isActive): void
    {
        $this->isActive = boolval($isActive);
    }
}

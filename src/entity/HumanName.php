<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

use function sprintf;

trait HumanName
{
    use Name;

    #[ORM\Column]
    private string $surname = '';

    /**
     * First "surname"
     */
    public function getFullNameReverse(): string
    {
        return $this->getFullName(true);
    }

    /**
     * First "name" OR with $reverse[true] first "surname"
     */
    public function getFullName(bool $reverse = false): string
    {
        if ($reverse) {
            return sprintf('%s %s', $this->getSurname(), $this->getName());
        }

        return sprintf('%s %s', $this->getName(), $this->getSurname());
    }

    public function getSurname(): string
    {
        return $this->surname;
    }
}

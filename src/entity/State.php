<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\ORM\Mapping as ORM;

trait State
{
    #[ORM\Column]
    private int $state;

    public function getState(): ?int
    {
        return $this->state;
    }
}

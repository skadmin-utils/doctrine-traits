<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\DateTime;

use function sprintf;

trait Validity
{
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?DateTimeInterface $validityFrom = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $validityTo = null;

    public function getValidityFrom(): DateTimeInterface
    {
        return $this->validityFrom;
    }

    public function getValidityTo(bool $force = false): ?DateTimeInterface
    {
        if ($force || $this->validityTo !== null) {
            return $this->validityTo;
        }

        return (new DateTime())->modify('+1 year');
    }

    public function getValidityFormat(string $from = 'd.m.Y H:i:s', string $to = 'd.m.Y H:i:s', string $format = '%s - %s'): string
    {
        return sprintf($format, $this->getValidityFrom()->format($from), $this->getValidityTo()->format($to));
    }
}

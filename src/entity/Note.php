<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

trait Note
{
    #[ORM\Column(type: Types::TEXT)]
    private string $note;

    public function getNote(): string
    {
        return $this->note;
    }
}

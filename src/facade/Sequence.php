<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Facade;

use function intval;

trait Sequence
{
    public function getValidSequence(): int
    {
        $sequence = $this->getModel()
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        return intval($sequence);
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Facade;

use function date;
use function str_pad;

trait OrderNumber
{
    public function getValidOrderNumber(?int $prefix = null): string
    {
        if ($prefix === null) {
            $prefix = date('Ym');
        }

        $orderNumber = $this->em
            ->getRepository($this->table)
            ->createQueryBuilder('a')
            ->select('max(a.orderNumber) as orderNumber')
            ->where('a.orderNumber LIKE :orderNumber')
            ->setParameter('orderNumber', $prefix . '%')
            ->getQuery()
            ->getSingleResult()['orderNumber'];

        if ($orderNumber === null) {
            $orderNumber = str_pad((string) $prefix, 10, '0');
        } else {
            $orderNumber++;
        }

        return (string) $orderNumber;
    }
}

<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Facade;

use Nette\Utils\Random;

trait Hash
{
    public function getValidHash(): string
    {
        $hash = Random::generate(32);
        while ($this->existWithHash($hash)) {
            $hash = Random::generate(32);
        }

        return $hash;
    }

    public function existWithHash(string $hash): bool
    {
        $criteria = ['hash' => $hash];

        $object = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $object !== null;
    }
}

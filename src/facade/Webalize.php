<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits\Facade;

use Nette\Utils\Strings;

use function sprintf;
use function strip_tags;

trait Webalize
{
    public function getValidWebalize(string $name): string
    {
        $webalizeName = Strings::webalize(strip_tags($name));
        $webalize     = $webalizeName;
        $cnt          = 2;
        while ($this->existWithWebalize($webalize)) {
            $webalize = sprintf('%s-%d', $webalizeName, $cnt++);
        }

        return $webalize;
    }

    public function existWithWebalize(string $webalize): bool
    {
        $criteria = ['webalize' => $webalize];

        $tournament = $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);

        return $tournament !== null;
    }
}

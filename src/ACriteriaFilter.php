<?php

declare(strict_types=1);

namespace SkadminUtils\DoctrineTraits;

use Doctrine\Common\Collections\Criteria;

use function sprintf;

abstract class ACriteriaFilter
{
    abstract public function modifyCriteria(Criteria &$criteria, string $alias = 'a'): void;

    protected function getEntityName(string $alias, string $column): string
    {
        return sprintf('%s.%s', $alias, $column);
    }
}

##### replace
```text
from: "App\Model\Doctrine\ACriteriaFilter"
to: "SkadminUtils\DoctrineTraits\ACriteriaFilter"

from: "App\Model\Doctrine\Facade"
to: "SkadminUtils\DoctrineTraits\Facade"
```